/* app.js */

var express = require('express'); // сама вся эта оболочка, шаблон для упрощенной разработки
var cookieParser = require('cookie-parser'); // Разобрать заголовок cookie и заполнить req.cookies объектом
var morgan = require('morgan'); // логгер
var cors = require('cors'); // конфликт с хромом решаю, чтобы не блочил запросы слкх

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

app.use(morgan('dev')); // вот так вот логгер пишет мне цветные сообщения в консоли на каждый запрос используя еще debug пакет
// app.use(morgan('combined', {
//     skip: function (req, res) { return res.statusCode < 400 }
// }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

app.use('/', indexRouter);
app.use('/user', usersRouter);



module.exports = app;
