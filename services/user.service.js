/* services/user.service.js */
const repository = require("../repositories/user.repository");

const addNewUser = (objUserInfo) => {
  // Проверяю есть ли поле _id, если есть проверяю чтобы превращалось в число,
  if( objUserInfo.hasOwnProperty('_id') && !isNaN(objUserInfo['_id']) ) {
    return repository.saveUserInfo(objUserInfo);
  } else {
    return null;
  }
};

const getAllUsers = () => {
  return repository.getUserList();
}

const getUser = (id) => {
  if(isNaN(id)) { // Преобразовать к числу, откинуть все запросы в которых нет чисел
    return null;
  } else {
    return repository.getUserById(id); // И только в том случае обратимся к репозитрию
  }
}

const changeUserInfo = (objUserInfo, setID) => {
  // Если id можно превратить в число, значит всё норм.
  if(!isNaN(setID)) {
    // перезаписываем свойство или же просто добавляем, не важно
    objUserInfo['_id'] = setID;
    return repository.saveChangesUser(objUserInfo);
  } else {
    return null;
  }
};

const deleteUser = (id) => {
  if(!isNaN(id)) {
    return repository.deleteUserById(id);
  } else {
    return null;
  }
};

module.exports = {
  addNewUser,
  getAllUsers,
  getUser,
  changeUserInfo,
  deleteUser
};