/* repositories/user.repository.js */

const fs = require('fs'); // подгружаем файловую систему в боооой
const pathToFile = './userlist.json';
const file = fs.readFileSync(pathToFile, 'utf-8');
let userList = JSON.parse(file); // парс джсона, можно теперь менять инфу


function saveChangesToFile(pathToFile, userList) { // Вдруг много файлов будет
  const newFile = JSON.stringify(userList);
  fs.writeFileSync(pathToFile, newFile, 'utf-8'); // записываем всё это в файл
}


const saveUserInfo = (objUser) => {
  // Проверяю если есть в userList пользователь с таким id, ошибочка
  if(userList.find(element => element._id == objUser['_id'])) {
    return;
  } else {
    userList.push(objUser); // добавляем в переменную userlist
    saveChangesToFile(pathToFile, userList);
    return objUser['_id']; // вернем id для красивого статуса запроса
  }
};

const getUserList = () => {
  return userList;
};

const getUserById = (id) => {
  // Тут мы можем вернуть либо инфу по пользователю, либо undefined, что учитывается в routes
  return userList.find(element => element._id == id); 
};

const saveChangesUser = (objUserInfo) => {
  const index = userList.findIndex(element => element._id == objUserInfo['_id']);
  // проверка если нет в хранилище юзера с таким индексом
  // Такая проверка, потому что индекс может быть 0, а 0 дал бы false и я не могу писать
  // if (index), потому пишу немного странно
  if(index === -1) {
    return;
  }
  userList[index] = objUserInfo;
  saveChangesToFile(pathToFile, userList);
  return objUserInfo['_id'];
};

const deleteUserById = (id) => {
  const index = userList.findIndex(element => element._id == id);
  if(index === -1) {
    return;
  }
  userList.splice(index, 1);
  saveChangesToFile(pathToFile, userList);
  return id;
};

module.exports = {
  saveUserInfo,
  getUserList,
  getUserById,
  saveChangesUser,
  deleteUserById
};