/* routes/users.js */

var express = require('express');
var router = express.Router();

const service = require("../services/user.service");

router.get('/', function(req, res) {
  const result = service.getAllUsers();

  res.send(result);
});

router.get('/:id', function(req, res) {
  const result = service.getUser(req.params.id);
  
  if(result) {
    res.send(result);
  } else if(result === null) { // Если в запросе написана какая-то бурда, не число
    res.status(404).send('Request can consist only of user ID');
  } else { // в остальных случаях, будет равно undefinded, тоесть функция find не нашла пользователя с таким ID
    res.status(500).send(`User with id ${req.params.id} does not exist`);
  }
});

router.post('/', function(req, res) {
  const result = service.addNewUser(req.body);
  // Эта проверка выполняется когда возвращается id, даже если id = 0, выполняется, любая непустая строка это true
  // Проверка на то чтобы эта строка превращалась в число, находится в services
  if(result) { 
    res.send(`User with id ${result} saved successfully`);
  } else if (result === null) {
    res.status(400).send('Valid _id property not found');
  } else {
    res.status(500).send('User with such id already exists, use PUT');
  }
});

router.put('/:id', function(req, res) {
  const result = service.changeUserInfo(req.body, req.params.id);
  if(result) {
    res.send(`User with id ${result} updated successfully`);
  } else if (result === null) {
    res.status(400).send('Valid id not found');
  } else {
    res.status(500).send('User with such id does not exist, use POST');
  }
});

router.delete('/:id', function(req, res) {
  const result = service.deleteUser(req.params.id);
  if(result) {
    res.send(`User with id ${result} deleted successfully`);
  } else if (result === null) {
    res.status(400).send('Valid id not found');
  } else {
    res.status(500).send('User with such id does not exist');
  }
});

module.exports = router;