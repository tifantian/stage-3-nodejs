# stage-3/NodeJS

## You can use this server at https://bsa19-stage3-nodejs.herokuapp.com

## Instalation

`npm install`

`npm start`

the server will automatically start **nodemon** 

**open** http://localhost:3000/

## This node.js server can receive requests:

```
GET: /user
GET: /user/:id
POST: /user
PUT: /user/:id
DELETE: /user/:id
```

### The server can recognize errors

- **GET**, **PUT** or **DELETE** request to user which does not exist
- ID property not found
- **POST** request recognizes when the id of the requested user does not exist

#### You can use project [my Project from stage 2 #bsa19](https://bitbucket.org/tifantian/stage-3-updated-stage-2-for-stage-3/src/master/) with users from server 

Do not forget look at **README.md** in that project.